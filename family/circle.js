var width_f;
var height_f;
var mouseX_f;
var mouseY_f;
var renderer_f;
var stage_f;
var uniforms_f = {};
var count_f = 0;
var shaderCode_f;
var smokeShader_f;
var bg_f;

var div_f;

var needToKill_f = false;
var graphicsClear_f = false;

function postScriptsLoad_f()
{
    mouseX_f = width_f / 2;
    mouseY_f = height_f / 2;
    renderer_f = new PIXI.autoDetectRenderer(width_f, height_f);
    document.getElementById(div_f).appendChild(renderer_f.view);
    //document.addEventListener('mousemove', function (e) {
    //    mouseX_f = e.clientX;
    //    mouseY_f = e.clientY;
    //}, false);

    stage_f = new PIXI.Container();


    uniforms_f.resolution = { type: 'v2', value: { x: width_f, y: height_f + 1000 } };
    uniforms_f.alpha = { type: '1f', value: 0.3 };
    uniforms_f.shift = { type: '1f', value: -800.6 };
    uniforms_f.time = { type: '1f', value: 1.0 };
    uniforms_f.speed = { type: 'v2', value: { x: -0.9, y: 0.9 } };
    uniforms_f.mouse = { type: 'v2', value: { x: mouseX_f, y: mouseY_f } };

    var fragmentShader = ['precision mediump float;',
      'uniform vec2      resolution;',
      'uniform float     time;',
      'uniform float     alpha;',
      'uniform vec2      speed;',
      'uniform float     shift;',
      'uniform float     mouse;',
      'vec2 fireSize = vec2(resolution.y / 1.0, resolution.y / 1.0);',
      'float rand(vec2 n) {',
      'return fract(cos(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);',
      '}',
      'float noise(vec2 n) {',
      'const vec2 d = vec2(.0, 1.);',
      'vec2 b = floor(n), f = smoothstep(vec2(.0), vec2(1.0), fract(n));',
      'return mix(mix(rand(b), rand(b + d.yx), f.x), mix(rand(b + d.xy), rand(b + d.yy), f.x), f.y);',
      '}',
      'float fbm(vec2 n) {',
      'float total = 0.1, amplitude = 2.0;',
      'for (int i = 0; i < 4; i++)',
      '{',
      'total += noise(n) * amplitude;',
      'n += n;',
      'amplitude *= 0.24;',
      '}',
      'return total;',
      '}',
      'void main() {',
      'const vec3 c1 = vec3(104./256., 115.0/256., 236./256.);',
      'const vec3 c2 = vec3(109./256., 72.0/256., 210./256.);',
      'vec2 p = gl_FragCoord.xy * 3.2 / resolution.yy;',
      'float q = fbm(p + fbm(p - time * 0.3));',
      'vec2  r = vec2(fbm(p + q + time * speed.x - p.x - p.y), fbm(p + q - time * speed.y));',
	  'vec3 c = mix(c1, c2, fbm(p + r));// + mix(c1, c2, r.x) - mix(c1, c2, r.y);',
      'float grad = gl_FragCoord.y / resolution.y;',
      'gl_FragColor = vec4(c, 0.0);',
      'gl_FragColor.xyz *= 1.0 - grad;',
      'gl_FragColor.a = 0.4;',
      '}'].join('\n');

    smokeShader_f = new PIXI.AbstractFilter('', fragmentShader, uniforms_f);

    bg_f = PIXI.Sprite.fromImage('');
    bg_f.width = width_f;
    bg_f.height = height_f;
    bg_f.filters = [smokeShader_f];

    stage_f.addChild(bg_f);
    animate_f();
}

function init_f(w, h, div)
{
    div_f = div;
    width_f = w;
    height_f = h;

    needToKill_f = false;
    graphicsClear_f = false;

    var s = document.createElement('script');
    s.setAttribute('src', "family/js/pixi.min.js");
    document.body.insertBefore(s, document.getElementById("familyscript_circle"));

    s.onload = function () {       
        postScriptsLoad_f();
    };  
}

function destroy_f() {
    document.getElementById(div_f).removeChild(renderer_f.view);
    stage_f.removeChild(bg_f);

    graphicsClear_f = true;
}

function resize_f(w, h)
{
   // console.log("resize", w, h);
    width_f = w;
    height_f = h;
    uniforms_f.resolution = { type: 'v2', value: { x: w, y: h } };//+ 1000 } };
    renderer_f.resize(w, h);

    bg_f.width = w;
    bg_f.height = h;

   // console.log("init1  " + window.innerWidth, window.innerHeight);
  //  console.log("init2  " + document.body.clientWidth, document.body.clientHeight);
}

function animate_f() {

    if (needToKill_f)
        return;

    requestAnimationFrame(animate_f);
    count_f += 0.002;
    smokeShader_f.uniforms.time.value = count_f;
    renderer_f.render(stage_f);
   // console.log("animate_circle");

    if (graphicsClear_f)
        needToKill_f = true;
}