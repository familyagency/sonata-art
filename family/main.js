var familyMainModule = (function() {

var _fragmentShader = [
  'uniform vec3 color;',
  'uniform sampler2D texture;',
  'varying vec3 vColor;',
  'varying float vAlpha;',
  'varying float vRotation;',
  'void main() {',
      'gl_FragColor = vec4( vColor, vAlpha );',
      'float mid = 0.5;',
      'vec2 rotated = vec2(cos(vRotation) * (gl_PointCoord.x - mid) + sin(vRotation) * (gl_PointCoord.y - mid) + mid,',
    'cos(vRotation) * (gl_PointCoord.y - mid) - sin(vRotation) * (gl_PointCoord.x - mid) + mid);',
    'vec4 rotatedTexture = texture2D( texture,  rotated);',
    'gl_FragColor = gl_FragColor * rotatedTexture;',
  '}'].join('\n');

var _vertexShader = [
     'attribute float size;',
     'attribute vec3 customColor;',
     'attribute float customAlpha;',
     'attribute float customRotation;',
     'attribute float direction;',
     'varying vec3 vColor;',
     'varying float vAlpha;',
     'varying float vRotation;',
     'bool increase = true;',
     'float MaxSize = 500.;',
     'float _size = 20.;',
     'void main() {',
        'vColor = customColor;',
        'vRotation = customRotation;',
        'vAlpha = customAlpha;',
        'vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );',
        'gl_PointSize = size * ( 400.0 / -mvPosition.z );',
        'gl_Position = projectionMatrix * mvPosition;',
      '}'].join('\n');

//=====================================================
//=====================================================
//=====================================================

//if (!Detector.webgl) Detector.addGetWebGLMessage();

var WIDTH;
var HEIGHT;

var cellsize = 10;
var currentImage;
var imageIndex = 0;

var mouseX = 0, mouseY = 0;
var windowHalfX = 0;
var windowHalfY = 0;

var needToKill = false;
var graphicsClear = false;

var effectType = 1;
var timer_3d = new clock.Clock();

var params =
    [{ cellsize: 6.0, maxBigScaleMeshes: 16, normalScale: 2.0 },//���������

    { cellsize: 50.7, maxBigScaleMeshes: 16, normalScale: 2.0 },
    { cellsize: 3, maxBigScaleMeshes: 16, normalScale: 3 },// ������

    { cellsize: 5.7, maxBigScaleMeshes: 16, normalScale: 2.0 },// ����
    { cellsize: 8, maxBigScaleMeshes: 5, normalScale: 3 },
    { cellsize: 3.5, maxBigScaleMeshes: 16, normalScale: 3 }//�������
    ];

//=====================================================
//======================= LOADING =====================
//=====================================================
var divid;
var divFullscreen;
var isImageLoaded = false;

var polyOnly = false;
var furOnly = true;
var sphereOnly = false;

var requestId;

function init(w, h, id, _divid, divFull) {

    WIDTH = w;
    HEIGHT = h;
    windowHalfX = w / 2;
    windowHalfY = h / 2;

    imageIndex = id - 1;

    needToKill = false;
    graphicsClear = false;

    divid = _divid;

    if (divFull == undefined)
    {
        divFull = '.container_family';
    }

    divFullscreen = document.getElementById(divFull);   

    postScriptsLoad();
}

function postScriptsLoad()
{
    if (!isImageLoaded)
        familyImageStoreModule.preloadimages().done(function (images)
        {
            familyImageStoreModule.imageDataArray = [];           
            cellsize = 4.5;//params[imageIndex].cellsize;

            for (var k = 0; k < images.length; ++k)
            {               
               // console.log("---------------- ", cellsize);
                var artImage = images[k];
                var _rows = Math.round(artImage.width / cellsize);
                var _cols = Math.round(artImage.height / cellsize);
                var particles = _rows * _cols;
                var imagedata = familyImageStoreModule.getImageData(artImage);
                var index = 0;
                var _colorArray = new Array();
                for (var i = 0; i < _rows; i++) {
                    for (var j = 0; j < _cols; j++) {
                        var x = Math.round(i * cellsize);
                        var y = Math.round(j * cellsize);
                        var pixel = familyImageStoreModule.getPixel(imagedata, x, y);
                        _colorArray[index++] = pixel;
                    }
                }

                particles = index;

               // _colorArray = shuffleArray(_colorArray);
           
                familyImageStoreModule.imageDataArray.push({
                    image: artImage,
                    rows: _rows,
                    cols: _cols,
                    partcilesCount: particles,
                    colorArray: _colorArray,
                    flyingParticles: 0,
                    scale: (1.0 * artImage.width) / artImage.height
                })            
            }

            isImageLoaded = true;
            currentImage = familyImageStoreModule.imageDataArray[imageIndex];
            initScene();
        })
    else
    {
        currentImage = familyImageStoreModule.imageDataArray[imageIndex];
        initScene();
    }
}

//=====================================================
//==================== 3D =============================
//=====================================================

var particleMesh;
var camera;
var renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
var scene = new THREE.Scene();
var geometry;
var points = [];
var meshMouse;
var rayCaster;
var canvasPosition = {};
var allCount;
var objectPositionZ = 0;
var geometryPlane;

function initScene()
{
    //console.log("initScene   ", imageIndex);

    if (furOnly)
    {
        furinit(imageIndex);
        return;
    }

    if (sphereOnly)
    {
        sphereinit();
        return;
    }

    if (polyOnly)
    {
        polyinit();
        return;
    }

    ///////
    
    if (imageIndex == 1 || imageIndex == 4)
    {
        furinit(imageIndex);
        return;
    }
    else if (imageIndex == 2 || imageIndex == 5)
    {
         sphereinit();           
         return;
    }
    else
    {
        polyinit();
    }  
}

function init3D()
{
    // camera = new THREE.PerspectiveCamera(39, WIDTH / HEIGHT, 1, 10000);
    camera = new THREE.OrthographicCamera(WIDTH / -2, WIDTH / 2, HEIGHT / 2, HEIGHT / -2, 1, 10000);
    camera.rotateZ(1.57);
    camera.position.z = 1700;
    camera.position.y = 5;
    camera.position.x = 2;//y real

    scene.add(new THREE.AmbientLight(0xffffff, .5));

    var lights = [];
    lights[0] = new THREE.DirectionalLight(0xf7fcfb, 1);//0x11E8BB, 1);
    lights[0].position.set(0, 0, -10);

    lights[1] = new THREE.DirectionalLight(0xffffff, 4.5);//0x11E8BB, 1);
    lights[1].position.set(0.0, 10, -10);

    lights[2] = new THREE.DirectionalLight(0xffffff, 4.5);//0x8200C9, 1);
    lights[2].position.set(0., -10, 10);

    scene.add(lights[1]);
    scene.add(lights[2]);

    allCount = currentImage.partcilesCount;

    console.log(" allCount ", allCount);

    //======================= mouse mimic ===================

    var materialRed = new THREE.MeshPhongMaterial({
        color: 0xff0000,
        shading: THREE.FlatShading,
        side: THREE.DoubleSide,
        transparent: true,
        opacity: 0
    });

    var materialBlue = new THREE.MeshPhongMaterial({
        color: 0x0000ff,
        shading: THREE.FlatShading,
        side: THREE.DoubleSide
    });

    var geometryMouse = new THREE.BoxBufferGeometry(20, 20, 20);

    meshMouse = new THREE.Mesh(geometryMouse, materialBlue);
    meshMouse.position.set(-10000, -10000, objectPositionZ);

    geometryPlane = new THREE.PlaneGeometry(4000, 4000, 32);
    var plane = new THREE.Mesh(geometryPlane, materialRed);
    scene.add(plane);
    //=====================================================
}

//=====================================================
//==================== Polygons  ======================
//=====================================================

function polyinit()
{
    setEffectType(1);
    init3D();  

    var material = new THREE.MeshPhongMaterial({
        color: 0xffffff,
        shading: THREE.FlatShading
    });

    material = new THREE.MeshPhongMaterial({
        color: 0x837beb,
        shading: THREE.FlatShading,
        transparent: true
    });

    // main particles    
    geometry = new THREE.BufferGeometry();

    points = [];

    var geometryMesh = new THREE.TetrahedronGeometry(4, 0);
    // geometryMesh = new THREE.TorusKnotGeometry(3, 1, 100, 16);
    // geometryMesh = new THREE.SphereGeometry(3, 30, 30);

    particleMesh = new THREE.Object3D();

    var bigcounter = 0, j = 0;
    var maxBigScaleMeshes = 50;

    for (var k = 0; k < allCount; k += 1) {

        if (currentImage.colorArray[k].has == false)
            continue;

        var x = cellsize * (k % currentImage.cols) - cellsize * currentImage.cols * 0.5 + Math.random() * 15;
        var y = (currentImage.scale * cellsize) * (k / currentImage.rows) - cellsize * currentImage.rows / 2 + Math.random() * 15;
       // material.opacity = opacityGet();       
        
        var _mesh = new THREE.Mesh(geometryMesh, material);
        var normalScale = true;

        if (bigcounter < params[imageIndex].maxBigScaleMeshes && (j++ % 100) == 0) {
            bigcounter++;
            normalScale = false;
        }

        var particle = new FamilyPolyParticle();
        particle.mesh = _mesh;
        particle.material = material;

        particle.setRandomRotation();
        normalScale ? particle.setNormalScale(params[imageIndex].normalScale) : particle.setlargeScale();
       // particle.setNormalScale(1)

        particle.setXYZ(x, y, objectPositionZ + getRandomArbitrary(-100, 100));// - Math.random()*100);
        particle.initDefaults();
        points.push(particle);

        particleMesh.add(_mesh);
    }  

    scene.add(particleMesh);

    //scene.add(meshMouse);

    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setClearColor(new THREE.Color(0xFF0000), 0);
    renderer.setSize(WIDTH, HEIGHT);
    renderer.autoClear = true;

    familyImageMaskModule.initMask(imageIndex, divid);
    
    divFullscreen.appendChild(renderer.domElement);   

    document.addEventListener('mousemove', onDocumentMouseMove, false);
    animate();
    resizePre(WIDTH, HEIGHT);

    //startInitAnimation();
    initAnimationComplete = true;
}

var animStartTime = 0;
var initAnimationComplete = false;

function startInitAnimation()
{
    initAnimationComplete = false;

    var coords = { x: 0, y: -700 }; // Start at (0, 0)
    var tween = new TWEEN.Tween(coords) // Create a new tween that modifies 'coords'.
            .to({ x: 0, y: 700 }, 2000) // Move to (300, 200) in 1 second.
            .easing(TWEEN.Easing.Quadratic.Out) // Use an easing function to make the animation smooth.
            .onUpdate(function () { 
                meshMouse.position.x = coords.x;
                meshMouse.position.y = coords.y;
            })
             .onComplete(function(){
               //  console.log("-------------- complete --------------");
                 initAnimationComplete = true;
                
             })
            .start(); // Start the tween immediately.
}

function opacityGet()
{
    var rand = getRandomArbitrary(0., 1.);
    var opacity = 1.;

    if (rand < 0.3)
        opacity = 0.1 + getRandomArbitrary(0.2, 0.8);
    else if (rand >= 0.3 && rand <= 0.4)
        opacity = 0.3 + getRandomArbitrary(0.1, .6);
    else if (rand >= 0.4 && rand <= 0.97)
        opacity = 0.3 + getRandomArbitrary(0.2, 0.7);
    else
        opacity = 0.3 + getRandomArbitrary(.5, .9);

    return opacity;
}

//=====================================================
//==================== Spheres  =======================
//=====================================================

function sphereinit()
{
    setEffectType(2);

    initAnimationComplete = true;
    circlesEffectModule.init_circle(imageIndex, divid);
    familyImageMaskModule.initMask(imageIndex, divid);   
   // particleSystem = new THREE.Points(createGeometryFromImage(currentImage), getShaderMaterial());
   // scene.add(particleSystem);
}

function getShaderMaterial() {
    var shaderMaterial = new THREE.ShaderMaterial({
        uniforms: uniforms,
        vertexShader: _vertexShader,//document.getElementById('vertexshader').textContent,
        fragmentShader: _fragmentShader,//document.getElementById('fragmentshader').textContent,
        blending: THREE.AdditiveBlending,
       // depthTest: true,
        transparent: true
    });

    return shaderMaterial;
}

function createGeometryFromImage(imageData)
{
    uniforms = {
        color: { value: new THREE.Color(0xffffff) },
        texture: { value: new THREE.TextureLoader().load(particleMaterial) }
    };

    var phongMaterial = new THREE.MeshPhongMaterial({
        color: 0xaaaaaa, specular: 0xffffff, shininess: 250,
        side: THREE.DoubleSide, vertexColors: THREE.VertexColors
    });

    geometry = new THREE.BufferGeometry();

    allCount = currentImage.partcilesCount;
    
    var colors = new Float32Array(allCount * 3);
    var sizes = new Float32Array(allCount);
    var rotations = new Float32Array(allCount);
    var alphas = new Float32Array(allCount);  
    var finalPositions = new Float32Array(allCount);
    var color = new THREE.Color();
   var currentColor = new THREE.Color();

    var col1 = new THREE.Color();
    col1.setRGB(255 / 255, 255 / 255, 255 / 255);
    currentColor = col1;

    points = [];

    var ind = 0;
    for (var i = 0, i3 = 0; i < allCount; i++)
    {
        if (imageData.colorArray[i].has == false)
            continue;

        var _x = cellsize * (i % imageData.cols) - cellsize * imageData.cols * 0.5 + Math.random() * 15;
        var _y = (imageData.scale * cellsize) * (i / imageData.rows) - cellsize * imageData.rows / 2 + Math.random() * 15;
        var _z = objectPositionZ;

        rotations[ind] = getRandomArbitrary(10.0, 200.0);
        alphas[ind] = getRandomArbitrary(0.0, 1.0);
        sizes[ind] = Math.random() > 0.8 ? 35 + 19 * Math.random() : 12 + 8 * Math.random();

       // color.setHSL(currentColor.getHSL().h, imageData.colorArray[i].g / 255., currentColor.getHSL().l);
        colors[i3 + 0] = 1;//color.r;
        colors[i3 + 1] = 1;//color.g;
        colors[i3 + 2] = 1;//color.b;

        var particle = new CircleParticle();
        particle.setXYZ(_x, _y, _z);
        particle.initDefaults();
        particle.setSize(sizes[ind]);

        points.push(particle);

        i3 += 3;
        ind++;
    }

    var positions = new Float32Array(points.length * 3);
    for (var i = 0, i3 = 0; i < points.length; i++, i3 += 3)
    {
        positions[i3 + 0] = points[i].getX();
        positions[i3 + 1] = points[i].getY();
        positions[i3 + 2] = points[i].getZ();
    }

    geometry.addAttribute('position', new THREE.BufferAttribute(positions, 3));
    geometry.addAttribute('customColor', new THREE.BufferAttribute(colors, 3));
    geometry.addAttribute('size', new THREE.BufferAttribute(sizes, 1));
    geometry.addAttribute('customAlpha', new THREE.BufferAttribute(alphas, 1));
    geometry.addAttribute('customRotation', new THREE.BufferAttribute(rotations, 1));

    return geometry;
}

//=====================================================
//==================== Fur ============================
//=====================================================

function furinit(id)
{
    setEffectType(3);

   // FurEffectModule.init_fur(id, divid);
    NoiseEffectModule.init(id, divid);
}

//=====================================================
//==================== RENDER LOOP  ===================
//=====================================================

function animate()
{
    //timer_3d.start();
    //timer_3d.ontick = function (td) {
    //    render();
    //}

    if (needToKill)
        return;

    render();
    requestId = requestAnimationFrame(animate);

    var time = performance.now() - animStartTime;   
    TWEEN.update(time);

    if (graphicsClear)
         needToKill = true;
}

function dist(x, y, x1, y1) {
    return Math.sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1));
}

function render() {
    
    var time = performance.now();
   // console.log("animte", time);
    if (effectType == 1) {
       // console.log(points.length);
        for (var k = 0; k < points.length; k += 1) {
            points[k].applyForce(meshMouse.position.x, meshMouse.position.y);
            points[k].update(time);
        }
    }
    else if (effectType == 2)
    {
       // var position = geometry.attributes.position.array;
       // var alphas = geometry.attributes.customAlpha.array;
       // var sizes = geometry.attributes.size.array;

       //// console.log(points.length, position.length / 3)
       // for (var k = 0, k3 = 0; k < points.length; k += 1, k3 += 3)
       // {
       //     points[k].applyForce(meshMouse.position.x, meshMouse.position.y);
       //     points[k].update(time);
           
       //     position[k3 + 0]  = points[k].getX();
       //     position[k3 + 1]  = points[k].getY();
       //     position[k3 + 2]  = points[k].getZ();
       //     alphas[k] = points[k].alpha;
       //    // console.log(alphas[k]);
            

       //     var _dist = this.dist(points[k].homeX1, points[k].homeY1, meshMouse.position.x, meshMouse.position.y);
       //     sizes[k] = clamp(10000 / (_dist * _dist), 20, 30);
       //     // this.newScale = 1000/_dist ;
       // }
      // console.log(points[0].getX());
        geometry.attributes.position.needsUpdate = true;
        geometry.attributes.customAlpha.needsUpdate = true;
        geometry.attributes.size.needsUpdate = true;
    }

    renderer.render(scene, camera);
}

//=====================================================
//==================== DOCUMENT FUNCS =================
//=====================================================    

function getClicked3DPoint(event)
{
    event.preventDefault();

    var raycaster = new THREE.Raycaster(); // create once
    var mouse = new THREE.Vector2(); // create once

    mouse.x =  (event.clientX / renderer.domElement.clientWidth)  * 2 - 1;
    mouse.y = -(event.clientY / renderer.domElement.clientHeight) * 2 + 1;

    if (!initAnimationComplete) return;

    raycaster.setFromCamera(mouse, camera);  
    var intersects = raycaster.intersectObjects(scene.children);

    for (var i = 0; i < intersects.length; i++) {

        if (intersects[i].object.geometry.type == "PlaneGeometry")
        {
            meshMouse.position.x = intersects[i].point.x;
            meshMouse.position.y = intersects[i].point.y;
          //  console.log(meshMouse.position.x);
        }      
    }    
}

function onDocumentMouseMove(event) {

    mouseX = (event.clientX - windowHalfX);
    mouseY = (event.clientY - windowHalfY);

    getClicked3DPoint(event);

    familyMouseModule.checkSpeed(mouseX, mouseY);
}

function resizePre(w, h) {

   // return;

    renderer.setSize(w, h);

    var camFactor = 2;
    var _scale = familyCanvasToolsModule.getScale();

    if (_scale == 1) {
        camFactor = 1.97;
        //console.log("===================== 1");
    }
    else if (_scale == 910 / 1170.) {
        camFactor = 1.5;
       // console.log("===================== 2");
    }
    else {

        if (_scale > 0.6)
            camFactor = 0.3 + _scale;
        else if (_scale > 0.5 && _scale < 0.6)
            camFactor = 0.55 + _scale;
        else if (_scale > 0.3 && _scale < 0.4)
            camFactor = 0.4 + _scale;
        else
            camFactor = 0.38 + _scale;

       // console.log("===================== 3", _scale);
    }

    camera.left = -w / camFactor;
    camera.right = w / camFactor;
    camera.top = h / camFactor;
    camera.bottom = -h / camFactor;
    camera.updateProjectionMatrix();
}

function setEffectType(id)
{
    effectType = id;
    familyImageMaskModule.setEffectType(effectType);
}

function resize(w, h)
{
    console.log("effectType ", effectType);
    if (effectType == 3)
    {
       // FurEffectModule.resizefurInit();
        NoiseEffectModule.resizeInit();
        return;
    }

    if (effectType == 2) {
        circlesEffectModule.resizeCircleInit();
        familyImageMaskModule.resizeImageInit();
        return;
    }

    familyImageMaskModule.resizeImageInit();
    resizePre(w, h);   
}

function destroy()
{
    if (effectType == 3)
    {
       // FurEffectModule.destroyFur();
        NoiseEffectModule.destroy();
        return;
    }

    if (effectType == 1 || effectType == 2)
    {
        familyImageMaskModule.destroyImageFont();
    }

    if (effectType == 2) {
        circlesEffectModule.destroy();
        return;
    }


    while (scene.children.length > 0) {
        //console.log("kill");
        scene.remove(scene.children[0]);
    }
   // console.log("============================KILL============================");

    //this.scene = null;  
    this.camera = null;
    
    
    cancelAnimationFrame(requestId);
    graphicsClear = true;
    document.removeEventListener('mousemove', onDocumentMouseMove, false);
    divFullscreen.removeChild(renderer.domElement);

   // timer_3d.stop();
}

function animateVector3(vectorToAnimate, target, options) {
    options = options || {};
    // get targets from options or set to defaults
    var to = target || THREE.Vector3(),
        easing = options.easing || TWEEN.Easing.Quadratic.In,
        duration = options.duration || 2000;
    // create the tween

    var tweenVector3 = new TWEEN.Tween(vectorToAnimate)
        .to({ x: to.x, y: to.y, z: to.z, }, duration)
        .easing(easing)
        .onUpdate(function (d) {
            if (options.update) {
                options.update(d);               
            }
        })
        .onComplete(function () {
            if (options.callback) options.callback();
        });
    // start the tween
    tweenVector3.start();
    // return the tween in case we want to manipulate it later on
    return tweenVector3;
}

return { 
    init: function (w, h, id, _divid, divFull) {
        init(w, h, id, _divid, divFull)
    },

    destroy: function () {
        destroy();
    },

    resize: function (w, h) {
        resize(w, h);
    },
    initAnimationComplete: function () {
        return initAnimationComplete
    },
}
}());