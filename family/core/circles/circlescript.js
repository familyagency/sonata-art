var circlesEffectModule = (function() {

var e3_resolution = 7;//8
var e3_mousePos = { x: 0, y: 0 };
var e3_timer = new clock.Clock();

var e3_lastMousemoveTime = 0;

//var canvasW = 1268;
var e3_canvasH = 190;
var e3_canvasW;
var e3_imagescale = 1.;

var e3_imageObj;
var e3_idIndex;
var e3_canvases = [];
var e3_ctxes = [];
var e3_divMain;

var pointParticles = [];
var loadedAll = 0;
var e3_time = 0;
var NUM_METABALLS = 180;
var metaballs = [];

var shiftVector = { x: 100, y: 100 };

function init_circle(e3_id, div)
{
    e3_divMain = div;
    e3_canvasW = document.getElementById(e3_divMain).clientWidth; //1268;    
    e3_imageObj = new Image();   

    e3_imageObj.onload = function ()
    {
        startAll();
    }
   
    e3_idIndex = e3_id;
    e3_imageObj.src = familyImageStoreModule.getImageRaw(e3_id);
}
    
function startAll()
{
   // if (++loadedAll < 2)
   //     return;

    var data = familyCanvasToolsModule.createCanvases(e3_divMain, 1);
    e3_ctxes = data.ctx;
    e3_canvases = data.cv;
    main_circle(e3_canvasW, e3_canvasH);

    window.addEventListener('mousemove', e3_mousemove, false);
    resizeCircleInit();
}


function e3_mousemove(evt)
{    
    e3_mousePos = familyCanvasToolsModule.getMousePos(e3_canvases[0], evt);
    //if (evt.timeStamp - lastMousemoveTime > 20)
    //{      
    //  //addUsersParticles();
    //}
    //e3_lastMousemoveTime = evt.timeStamp;
}

function destroy()
{
    e3_timer.stop(); 
    e3_ctxes[0].clearRect(0, 0, e3_canvases[0].width, e3_canvases[0].height);
    window.removeEventListener('mousemove', e3_mousemove, false);
    document.getElementById(e3_divMain).removeChild(e3_canvases[0]);
}

function e3_resizeListener()
{  
    resize(e3_canvasW, e3_canvasH);
}

function resizeCircleInit()
{
    resizeCircle(familyCanvasToolsModule.getScale());
}

function resizeCircle(_scale)
{  
    var w = e3_imageObj.width * _scale;
    var h = e3_imageObj.height * _scale;

    e3_imagescale = _scale;
    familyCanvasToolsModule.setCanvasSize(w + shiftVector.x, h + shiftVector.y, e3_canvases);
    metaCanvasEffectModule.setCanvasSize(w + shiftVector.x, h + shiftVector.y);
    familyCanvasToolsModule.resizeCanvases();
    _initText()
    var _canv = e3_canvases[0];
    metaCanvasEffectModule.setupMeta(_canv, pointParticles);
}

function main_circle(w, h)
{
    familyCanvasToolsModule.setCanvasSize(w, h, e3_canvases);
    _initText();
    
    e3_timer.start();
    e3_time = 0;
    // return;
    var _ctx = e3_ctxes[0];
    var _canv = e3_canvases[0];   

    metaCanvasEffectModule.setupMeta(_canv, pointParticles);
   
    e3_timer.ontick = function (td)
    { 
        _ctx.clearRect(0, 0, _canv.width, _canv.height);
        _ctx.filter = 'blur(15px)';
        metaCanvasEffectModule.drawMeta(e3_mousePos, shiftVector);      
    }    
}

function _initText() {
    var canv = e3_canvases[0];
    var _ctx = e3_ctxes[0];

    _ctx.drawImage(e3_imageObj, 0, 0, e3_imageObj.width * e3_imagescale, e3_imageObj.height * e3_imagescale);
    var imageData = _ctx.getImageData(0, 0, e3_imageObj.width * e3_imagescale, e3_imageObj.height * e3_imagescale);

    pointParticles = [];
   
    for (var x = 0; x < imageData.width; x += e3_resolution) {
        for (var y = 0; y < imageData.height; y += e3_resolution) {
            i = (y * imageData.width + x) * 4;
            if (imageData.data[i + 3] > 228) {
                pointParticles.push({ x: x, y: y });
            }
        }
    }    
}

return {
    init_circle: function (a, b) {
        return init_circle(a, b);
    },
    resizeCircleInit: function () {
        return resizeCircleInit();
    },
    destroy: function () {
        return destroy();
    },
}
}());