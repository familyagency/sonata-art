﻿var metaCanvasEffectModule = (function() {

var store = [];
var metawidth;
var metaheight;
var metacanvas;
var metactx;
var temp_canvas = document.createElement("canvas");
var temp_ctx = temp_canvas.getContext("2d");
var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
	window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    window.requestAnimationFrame = requestAnimationFrame;

var NUMBER_OF_LARD = 10;
const MAX_RADIUS = 7;//15 / 0.7;
const MIN_RADIUS = 0.1;//8 / 0.7;
const THRESHOLD = 2;

var metatime = 0;

var metaPoint;

    function setupMeta(canv, points) {
        metacanvas = canv;
        metactx = metacanvas.getContext("2d");
        metawidth =  temp_canvas.width = metacanvas.width;
        metaheight = temp_canvas.height = metacanvas.height;
        metaPoint = points;
        NUMBER_OF_LARD = metaPoint.length;// -100;
        metatime = 0;
        initMeta();
       // drawMeta();
    }

    function setCanvasSize(w, h) {
        metawidth = temp_canvas.width = metacanvas.width = w;
        metaheight = temp_canvas.height = metacanvas.height = h;
    }
    

    function initMeta() {
        store = [];

        for (let i = 0; i < NUMBER_OF_LARD; i += 1) {
            let radius = Math.random() > 0.1 ? getRandomArbitrary(MIN_RADIUS, MAX_RADIUS) : 4;
            let x = metaPoint[i].x;//Math.floor(Math.random() * (metawidth + 1 - (radius * 2)) + radius);
            let y = metaPoint[i].y;// Math.floor(Math.random() * (metaheight + 1 - (radius * 2)) + radius);
            let lard = new Lard({ x: x + 50, y: y + 50 }, radius, temp_ctx, MAX_RADIUS, MIN_RADIUS);
            store.push(lard);
        }

        for (let i = 0; i < 270; i += 1) {
            let radius = Math.random() * 2;
            let x = getRandomArbitrary(0, metawidth)
            let y = getRandomArbitrary(0, metaheight)
            let lard = new Lard({ x: x + 50, y: y + 50 }, radius, temp_ctx, Math.random()*3, 2);
            store.push(lard);
        }
    }

    function drawMeta(mouse, shiftVector) {
       // requestAnimationFrame(drawMeta);
        
        temp_ctx.clearRect(0, 0, metacanvas.width, metacanvas.height);
        for (let i = 0, iz = store.length; i < iz; i += 1) {
            store[i].draw(mouse, shiftVector);
        }

        let imageData = temp_ctx.getImageData(0, 0, metacanvas.width, metacanvas.height);
        let pix = imageData.data;

        //for (let i = 0, iz = pix.length; i < iz; i += 4) {
        //    if (pix[i + 3] < THRESHOLD) {
        //       // pix[i + 1] = 266;
        //        pix[i + 3] = 0;
        //    } else {
        //        pix[i + 3] = 255;
        //       // pix[i + 1] = 235;
        //       // pix[i + 2] = 0;

        //    }
        //}
      //  metactx.globalAlpha = 0.02;
        metactx.putImageData(imageData, 0, 0);
       // metactx.globalAlpha = 1.;
    }

    function rotate_to_point(n, m) {
        var distance_x = n.x - m.x;
        var distance_y = n.y - m.y;
        var angle = Math.floor(Math.atan2(distance_x, distance_y) * 100) / 100;
        return angle;
    }

    class Lard {
        constructor(position, radius, ctx, maxr, minr) {
            this.position = position;
            this.radius = radius;
            this.maxr = maxr;
            this.minr = minr;
            this.saveRadius = radius;
            this.ctx = ctx;
            this.is_graunding = false;
            this.is_separating = false;
            this.is_independent = true;
            this.alpha = 0.7 + Math.random() * 0.5;
            this.radiusV = 0.08 + Math.random() * 0.03;
            this.timeDamp = 0.0001 + Math.random() * 0.0001;
            this.needUpdate =  Math.random() > 0.5 ? 0 : 1;
          // this.needUpdate = false;// Math.random() > 0.6 ? 0 : 1;

            this.radiusSign = Math.random() > 0.5 ? -1 : 1;

            this.paramCircle = 0;// +Math.random() * 50;
        }

        updateVelocity(time)
        {
            this.vx = this.needUpdate ? this.radiusV * Math.cos(this.alpha * time) : 0;
            this.vy = this.needUpdate ? this.radiusV * Math.sin(this.alpha * time) : 0;
        }

        draw(mouse, shiftVector)
        {
            var p = this.position;

            var die = false;
            var distance = this.dist(mouse.x, mouse.y, p.x, p.y);
            var maxthr = 180;
            if (distance > maxthr + this.paramCircle)
            {
                die = true;
               // return;
            }
            else
            {
                if(this.radius < 0)
                {
                    this.radius = this.saveRadius;
                }
            }

            if (this.radius < 0) {
                return;
            }

            var colors = this.colors;
            var radius = this.radius;

            // グラデーション
            this.ctx.beginPath();
            this.grad = this.ctx.createRadialGradient(
                p.x, p.y, 0,
                p.x, p.y, this.radius
            );

            var alpha = 0.5;
            if (distance > maxthr - 40)
                alpha = mapInterval(distance, 0, maxthr, 0, 1);

            this.grad.addColorStop(0, "rgba(255,255,255,1)");
            this.grad.addColorStop(1, "rgba(255,255,255," + alpha + ")");
            this.ctx.globalAlpha = this.alpha;
            this.ctx.fillStyle = this.grad;

           // this.ctx.shadowBlur = 0.1;
           // this.ctx.shadowColor = "white";

            this.ctx.lineWidth = 14;
           // this.ctx.arc(p.x + shiftVector.x, p.y + shiftVector.y, this.radius, 0, Math.PI * 2);
            this.ctx.arc(p.x , p.y, this.radius, 0, Math.PI * 2);
            this.ctx.fill();
            this.ctx.globalAlpha = 1.;

            if (die)
            {
                this.radius -= Math.random(0.0002);
                return;
            }

            this.updateVelocity(this.timeDamp * (metatime));
            metatime += 0.5;

            p.x += this.vx;
            p.y += this.vy;

            this.radius += 0.09 * this.radiusSign;

            if (this.radiusSign == 1 && this.radius > this.maxr)
            {
                this.radiusSign = -1;
               
            }
            else if (this.radiusSign == -1 && this.radius < 1)
            {
                this.radiusSign = 1;
            }
            this.position = p;


           
            //if (p.x < this.radius || p.x > metacanvas.width - this.radius) {
            //    this.vx *= -1;
            //}

            //if (p.y < this.radius || p.y > metacanvas.height - this.radius) {
            //    this.vy *= -1;
            //}
        }

        dist(x, y, x1, y1) {
            return Math.sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1));
        }
    }

    return {
        drawMeta: function (a, b) {
            return drawMeta(a, b);
        },
        setupMeta: function (a, b) {
            return setupMeta(a, b);
        },
        setCanvasSize: function (a, b) {
            return setCanvasSize(a, b);
        },
    }
}());