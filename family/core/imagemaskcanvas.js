var familyImageMaskModule = (function () {

var maskCircleSize = 135;
var maskCircleSizeInner = 105;
var rad;

var imageFont;
var idFontIndex;
var divFontMain;
var imageFontScale = 1.;
var canvasesFont = [];
var ctxesFont = [];
var mousePosFont;
var timerImageMask = new clock.Clock();

var effectType = 1;

var circles = [];

function setEffectType(id)
{
    effectType = id;

    if (effectType == 1)
    {
        maskCircleSize = 135;
        maskCircleSizeInner = 105;
    }
    else if (effectType == 2)
    {
        maskCircleSize = 180;
        maskCircleSizeInner = 160;
        rad = maskCircleSize;
    }
}

function initMask(_id, __divid)
{
    divFontMain = __divid;

    var canvasH = 190;
    var canvasW = document.getElementById(divFontMain).clientWidth; //1268;    
    imageFont = new Image();

    var row = 10;
    var col = 10;

    var shiftX = 10, shiftY = 10;
   // circles.push(new Circle(0, 0));
    circles = [];
    //return;
   // circles.push(new Circle(100, 100));
   // return;
    for (var i = 0; i < col; i++)
    {
        for (var j = 0; j < row; j++) {
            var x = i * shiftX;
            var y = j * shiftY;
            circles.push(new Circle(x, y));
        }
    }

    imageFont.onload = function ()
    {     
        var data = familyCanvasToolsModule.createCanvases(divFontMain, 1);
  
        ctxesFont = data.ctx;
        canvasesFont = data.cv;
        
        mainMaskImage(canvasW, canvasH);        
        window.addEventListener('mousemove', mousemoveFont, false);
        resizeImageInit();
    }
   
    idFontIndex = _id;
    
   // console.log(familyImageStoreModule);
    imageFont.src = familyImageStoreModule.getImageRaw(_id);
}


function clearCircle(x, y, radius) {
    ctxesFont[0].save();
    ctxesFont[0].globalCompositeOperation = 'destination-out';
    ctxesFont[0].beginPath();
    ctxesFont[0].arc(x, y, radius, 0, 2 * Math.PI, false);
    ctxesFont[0].fill();
    ctxesFont[0].restore();
};

function mousemoveFont(evt)
{    
    mousePosFont = familyCanvasToolsModule.getMousePos(canvasesFont[0], evt);
    clearCircle(mousePosFont.x, mousePosFont.y, 200);
    rad = 1000;
   
   
    for (var i = 0; i < circles.length; i++) {
        circles[i].reset();      
    }
}

function destroyImageFont()
{
    timerImageMask.stop();
   
    window.removeEventListener('mousemove', mousemoveFont, false);
    ctxesFont[0].clearRect(0, 0, canvasesFont[0].width, canvasesFont[0].height);
    document.getElementById(divFontMain).removeChild(canvasesFont[0]);
}

function resizeImageInit()
{
    resizeImageMask(familyCanvasToolsModule.getScale());
}

function resizeImageMask(_scale)
{  
    var w = imageFont.width * _scale;
    var h = imageFont.height * _scale;

    imageFontScale = _scale;
    familyCanvasToolsModule.setCanvasSize(w, h, canvasesFont);

    initTextFont();
    clearImageMaskData();

    familyCanvasToolsModule.resizeCanvases();
}

function initTextFont()
{
    ctxesFont[0].drawImage(imageFont, 0, 0, imageFont.width * imageFontScale, imageFont.height * imageFontScale);
}

function clearImageMaskData()
{
    familyCanvasToolsModule.clearCanvases(canvasesFont, ctxesFont);
    imgFontdata = ctxesFont[0].getImageData(0, 0, canvasesFont[0].clientWidth, canvasesFont[0].clientHeight);   
}

function mainMaskImage(w, h)
{
    familyCanvasToolsModule.setCanvasSize(w, h, canvasesFont);
    timerImageMask.start();
    initTextFont();
  
    timerImageMask.ontick = function (td)
    {
        var cnvs = canvasesFont[0];
        var _ctx1 = ctxesFont[0];        
        _ctx1.putImageData(imgFontdata, 0, 0);
        drawImageCircle();
        if (mousePosFont != undefined)
        familyMouseModule.checkSpeed(mousePosFont.x, mousePosFont.y);
        var averageSpeed = 0.5 * Math.abs((familyMouseModule.getSpeedX() + familyMouseModule.getSpeedY()));
      //  console.log(averageSpeed);
    }  

    clearImageMaskData();
}

function drawImageCircle()
{
    if (mousePosFont == undefined) return;
    if (!familyMainModule.initAnimationComplete) return;
    
    _drawImageCircle();
}

function _drawImageCircle()
{
    var _ctx1 = ctxesFont[0];

    _ctx1.save();
    var shift = 50;
    _ctx1.translate(mousePosFont.x - shift, mousePosFont.y - shift);
    var time = performance.now();
    for (var i = 0; i < circles.length; i++)
    {
        circles[i].update();
        circles[i].draw(_ctx1);
    }
    _ctx1.restore();
}

class Circle {
    constructor(x, y) {
        this.maxRadius = 45 + Math.random(15);
        this.minRadius = 0.01;
        this.radius = 1;
        this.x = x + getRandomArbitrary(-30, 30);
        this.y = y + getRandomArbitrary(-30, 30);
        this.time = 0;
        this.anim = true;
        this.timeDamp = 0.4 + Math.random(0.4);
        this.tween;
        this.alpha = 1;
        this.grad
        this.nextRadius;
        this.radiusV = getRandomArbitrary(2, 3);
    }

    update() {       
        var averageSpeed = 0.5 * Math.abs((familyMouseModule.getSpeedX() + familyMouseModule.getSpeedY()));
        this.nextRadius = this.maxRadius - mapIntervalClamp(averageSpeed, 0, 40, 0, this.maxRadius);
      //  console.log(this.nextRadius);
        var sign = Math.sign(this.nextRadius - this.radius);

        this.radius += sign * this.timeDamp;
        this.radius = clamp(this.radius, this.minRadius, this.maxRadius);
        
        //if (this.anim) {
        //    if (this.radius >= this.maxRadius - 1) {
        //        this.radius = this.maxRadius;
        //        this.anim = false;
        //    }
        //    else {
        //        this.radius = this.maxRadius * Math.abs(Math.sin(this.time * this.timeDamp));
        //    }
             
        //    this.time++;
        //}
    }

    reset()
    {
       // this.time = 0;
       // this.anim = true;
       // this.radius = 1;    
    }

    draw(_ctx1) 
    {       
        _ctx1.globalCompositeOperation = "destination-out";
        // _ctx1.globalCompositeOperation = 'source-over';    
        
        //this.x += this.radiusV * Math.cos(this.alpha * this.time);
       // this.y += this.radiusV * Math.sin(this.alpha * this.time);
       // this.time += 0.01;

         this.grad = _ctx1.createRadialGradient(this.x, this.y, 0, this.x, this.y, this.radius);

         this.grad.addColorStop(0, "rgba(255,0,0,1)");
         this.grad.addColorStop(1, "rgba(255,0,0,0)");
       
        // _ctx1.shadowBlur = 100;
        // _ctx1.shadowColor = "white";
        // _ctx1.fillStyle = 'green';

        _ctx1.beginPath();
        _ctx1.fillStyle = this.grad;
        _ctx1.lineWidth = 1;
        _ctx1.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
        _ctx1.fill();
        _ctx1.closePath();
        
       
    }
}

return { 
    initMask: function (_id, __divid) {
        initMask(_id, __divid)
   },

   resizeImageInit: function () {
       resizeImageInit();
    },

   destroyImageFont: function () {
            destroyImageFont()
   },

   setEffectType: function (id) {
       setEffectType(id)
   }
}
}());