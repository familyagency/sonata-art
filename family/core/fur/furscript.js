var FurEffectModule = (function() {

var resolution = 12;//8
var mousePos;
var mousedown = false;

var pixels = new Array();
var choosenpixels = new Array();
var timer = new clock.Clock();

var noiseCanvas, noise, imgdata, data, hdrdata, FloatArray, fadeimgdata, noiseimgdata;

var indexes = { current: 0, dead: 0 }, userindexes = { current: 0, dead: 0 }, noiseindexes = { current: 0, dead: 0 };
var particleincrement = 10;

var release = true;
var xTextShift = -500, yTextShift = 50;
var coordX = 10, coordY = 60;

var colors = [{r: 255, g: 255, b: 255 }, {r: 255, g: 0, b: 0 }, {r: 255, g: 255, b: 0 }];

var userparticles = [];
var noiseparticles = [];

var lastMousemoveTime = 0;

var particlesCountForFade = 3000;
var fadespeed = 2;
var maxUserParticles = 7000;

var writepixels = false;
var preloadpixels = true;

//var canvasW = 1268;
var canvasH = 190;
var canvasW;
var imagescale = 1.;

var particles = [],
    color = 'rgb(4, 1, 1)',
    composite = 'lighter',
    defaultOptions =
    {
        maxAge: 55,
        exposure: 13.5,
        damping: .35,
        noise: 4.2,
        fuzz: 0.1,
        intensity: 18.0,
        initialXVelocity: 70,
        initialYVelocity: 75,
        spawn: 120
    },

    defaultUserOptions =
    {
        maxAge: 55,
        exposure: 2.5,
        damping: .25,
        noise: 8.2,
        fuzz: 0.3,
        intensity: 20.0,
        initialXVelocity: 50,
        initialYVelocity: 45,
        spawn: 80
    },

    noiseOptions =
    {
        maxAge: 10,
        exposure: 8.5,
        damping: 0.05,
        noise: 6.9,
        fuzz: 4.1,
        intensity: 10.0,
        initialXVelocity: 5,
        initialYVelocity: 4,
        spawn: 20
    };


var imageObj;
var idIndex;
var canvases = [];
var ctxes = [];
var divMain;

function init_fur(_id, divId)
{
    divMain = divId;
    canvasW = document.getElementById(divMain).clientWidth; //1268;    
    imageObj = new Image();   

    imageObj.onload = function ()
    {
        var data = familyCanvasToolsModule.createCanvases(divMain, 3);
        ctxes = data.ctx;
        canvases = data.cv;
        main(canvasW, canvasH);

        window.addEventListener('mousemove', mousemove, false);
        resizefurInit();

        if (!release) {
            window.addEventListener('resize', resizeListener, false);
        }
    }
   
    idIndex = _id;   
    imageObj.src = familyImageStoreModule.getImageRaw(_id);
}

function mousemove(evt)
{    
    mousePos = familyCanvasToolsModule.getMousePos(canvases[0], evt);
   // console.log("mouse moveeeee  ", evt);
    if (evt.timeStamp - lastMousemoveTime > 20)
    {      
       addUsersParticles();
    }
    lastMousemoveTime = evt.timeStamp;
}

function destroyFur()
{
    timer.stop();

    if (!release) {
        window.removeEventListener('resize', resizeListener, false);
    }

    ctxes[0].clearRect(0, 0, canvases[0].width, canvases[0].height);
    ctxes[1].clearRect(0, 0, canvases[1].width, canvases[1].height);
    ctxes[2].clearRect(0, 0, canvases[2].width, canvases[2].height);

    window.removeEventListener('mousemove', mousemove, false);

    var _div = document.getElementById(divMain);
    for (var i = 0; i < 3; i += 1) {
        //try {
            _div.removeChild(canvases[i]);
       // }
       // catch (e) {
            //console.log("try block");
      //  }
    }   
}

function shuffle(a)
{
    var j, x, i;
    for (i = a.length; i; i--)
    {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}

function resizeListener()
{  
    resize(canvasW, canvasH);
}

function resizefurInit()
{
    resizefur(familyCanvasToolsModule.getScale());
}

function resizefur(_scale)
{  
    var w = imageObj.width * _scale;
    var h = imageObj.height * _scale;  

    imagescale = _scale;
    familyCanvasToolsModule.setCanvasSize(w, h, canvases);
   
    particles = [];
    indexes.current = 0;
    indexes.dead = 0;

    userparticles = [];
    userindexes.dead = 0;

    initText();
    clearData();

    familyCanvasToolsModule.resizeCanvases();
}

function Pixel(homeX, homeY)
{
    this.homeX = homeX;
    this.homeY = homeY;
}

function initText()
{
    var canv = canvases[0];
    var _ctx = ctxes[0];

    _ctx.drawImage(imageObj, 0, 0, imageObj.width * imagescale, imageObj.height * imagescale);
    var imageData = _ctx.getImageData(0, 0, imageObj.width * imagescale, imageObj.height * imagescale);
   
   var index = 0;
   pixels = [];  

    //if (!preloadpixels)
    //{
    //    for (var x = 0; x < imageData.width; x += resolution) {
    //        for (var y = 0; y < imageData.height; y += resolution) {
    //            i = (y * imageData.width + x) * 4;
    //            if (imageData.data[i + 3] > 228) {
    //                if (index >= pixels.length) {
    //                    pixels[index] = new Pixel(x, y);
    //                }              
    //                index++;
    //            }
    //        }
    //    }
    //    pixels.splice(index, pixels.length - index);
    //}
    //else
    {       
        for (var x = 0; x < 600; x += resolution)
        {
            pixels.push(new Pixel(randomInteger(0, canv.clientWidth + 100), randomInteger(0, canv.clientHeight + 100)));
        }

        for (var x = 0; x < 400; x += resolution)
        {
            pixels.push(new Pixel(randomInteger(0, canv.clientWidth), randomInteger(canv.clientHeight - 200, canv.clientHeight + 200)));
        }       
    }

     ///////////// write pixels   
    //if (writepixels)
    //{
    //    writepixels(pixels);
    //}
    /////////////////////////////////////////////

    var vx = defaultOptions.initialXVelocity,
        vy = defaultOptions.initialYVelocity;

    for (var j = 0; j < pixels.length; j++) {
        for (i = 0; i < defaultOptions.spawn; i++) {
            var greyamount = randomFloat(0.95, 1.0);
            particles.push({
                vx: fuzzy(vx),
                vy: fuzzy(vy),
                x: pixels[j].homeX * familyCanvasToolsModule.getPixelRatio(),
                y: pixels[j].homeY * familyCanvasToolsModule.getPixelRatio(),
                initx: pixels[j].homeX * familyCanvasToolsModule.getPixelRatio(),
                inity: pixels[j].homeX * familyCanvasToolsModule.getPixelRatio(),
                initvx: fuzzy(vx),
                initvy: fuzzy(vy),
                age: 0,
                color: { r: greyamount, g: greyamount, b: greyamount }
            });
        }
    }

    _ctx.clearRect(0, 0, canv.width, canv.height);
    var dup_array = particles.slice();

    shuffle(particles);
}

function fuzzy(range, base)
{
    return (base || 0) + (Math.random() - 0.5) * range * 2;
}

function clearData()
{
    familyCanvasToolsModule.clearCanvases(canvases, ctxes);
   
    imgdata = ctxes[0].getImageData(0, 0, canvases[0].clientWidth, canvases[0].clientHeight);
    fadeimgdata = ctxes[1].getImageData(0, 0, canvases[1].clientWidth, canvases[1].clientHeight);
    noiseimgdata = ctxes[2].getImageData(0, 0, canvases[2].clientWidth, canvases[2].clientHeight);

    data = imgdata.data;
    hdrdata = new FloatArray(data.length);
    for (var i = 0; i < hdrdata.length; i++) {
        hdrdata[i] = 0;
    }
}

function tonemap(n)
{
    return (1 - Math.pow(2, -n * 0.009 * defaultOptions.exposure)) * 255;
}

function getNoise(x, y, channel) {
    return noise[(~~x + ~~y * canvases[0].width) * 4 + channel] / 160 - 1.0;//127
}

function main(w, h)
{
    familyCanvasToolsModule.setCanvasSize(w, h, canvases);

    timer.start();

    noiseCanvas = makeOctaveNoise(canvases[0].clientWidth, canvases[0].clientHeight, 8);
    noise = noiseCanvas.getContext('2d').getImageData(0, 0, canvases[0].clientWidth, canvases[0].clientHeight).data;
    FloatArray = window.Float32Array || Array;
   
    initText(); 

    var lastdeadcount = 0;
   
   // return;
    timer.ontick = function (td)
    {
      
        if (mousedown)
        {
            addUsersParticles();
        }

        var cnvs = canvases[0];
        var _ctx1 = ctxes[0];
        var _ctx2 = ctxes[1];


        indexes.dead = updateParticles(indexes.dead, indexes.current, particles, cnvs, defaultOptions, data);
        userindexes.dead = updateParticles(userindexes.dead, userparticles.length, userparticles, cnvs, defaultUserOptions, data);
                  
        if (indexes.current + particleincrement < particles.length)
        {
            indexes.current += particleincrement;
        }
        //else
        //{
        //    indexes.current = particles.length - 1;
        //    recreateParticles();
        //}
               
       // console.log(indexes.dead);
       // if (indexes.dead != 0 && (indexes.dead > particlesCountForFade) && lastdeadcount != indexes.dead)
         if (indexes.dead > particlesCountForFade)
         {
            recreateParticles();
            lastdeadcount = indexes.dead;
            fadeimgdata = familyCanvasToolsModule.copyImageData(_ctx2, imgdata);

            for (var i = 0; i < imgdata.data.length; i += 4) {            
                imgdata.data[i + 3] = 0;
            }

            for (var i = 0; i < hdrdata.length; i++) {
                hdrdata[i] = 0;
            }

            userparticles = [];
            userindexes.dead = 0;
        }

        for (var i = 0; i < fadeimgdata.data.length; i += 4) {
            fadeimgdata.data[i + 3] -= fadespeed;
        }       


        _ctx1.globalCompositeOperation = 'source-over';
        _ctx1.putImageData(imgdata, 0, 0);
      
       _ctx1.globalCompositeOperation = "destination-in";
       _ctx1.drawImage(imageObj, 0, 0, imageObj.width * imagescale, imageObj.height * imagescale);
      // drawCircleMask();
       _ctx1.globalCompositeOperation = 'source-over';

        _ctx2.putImageData(fadeimgdata, 0, 0);
        _ctx2.globalCompositeOperation = "destination-in";
       // drawCircleMask();
        _ctx2.drawImage(imageObj, 0, 0, imageObj.width * imagescale, imageObj.height * imagescale);

    }  

    function updateParticles(index1, index2, _particles, cnvs, opt, data)
    {
        var w = cnvs.clientWidth * 1,
        h = cnvs.clientHeight * 1,
        maxAge = opt.maxAge,
        vx = opt.initialXVelocity,
        vy = opt.initialYVelocity,
        damping = opt.damping,
        noisy = opt.noise,
        fuzz = opt.fuzz,
        intensity = opt.intensity;

        var indexdeadsave = 0;

        for (var i = index1; i < index2; i++) {
            var p = _particles[i];
            p.vx = p.vx * damping + getNoise(p.x, p.y, 0) * 4 * noisy + fuzzy(0.1) * fuzz;
            p.vy = p.vy * damping + getNoise(p.x, p.y, 1) * 4 * noisy + fuzzy(0.1) * fuzz;
            p.age++;

            for (var j = 0; j < 10; j++) {
                p.x += p.vx * 0.1;
                p.y += p.vy * 0.1;

                if (p.x < 0 || p.x >= w || p.y < 0 || p.y >= h)
                    continue;

                var r = p.color.r, g = p.color.g, b = p.color.b;           

                var index = (~~p.x + ~~p.y * cnvs.width) * 4;
                data[index] = tonemap(hdrdata[index] += r * intensity);
                data[index + 1] = tonemap(hdrdata[index + 1] += g * intensity);
                data[index + 2] = tonemap(hdrdata[index + 2] += b * intensity);
                data[index + 3] = 255;
            }

            if (p.age >= maxAge) {
                indexdeadsave++;
            }
        }

        index1 += indexdeadsave;

        return index1;       
    }

    function  recreateParticles()
    {
        indexes.dead = 0;
        indexes.current = 0;

        for (var i = 0; i < particles.length; i++)
        {           
            particles[i].x = particles[i].initx;
            particles[i].y = particles[i].inity;
            particles[i].vx = particles[i].initvx;
            particles[i].vy = particles[i].initvy;
            particles[i].age = 0;
        }

        for (var i = 0; i < hdrdata.length; i++) {
            hdrdata[i] = 0;
        }

        particles = [];
        indexes.current = 0;
        indexes.dead = 0;
        initText();
    }    

     clearData();
}

function addUsersParticles()
{
    if (userparticles.length > maxUserParticles)
    {        
        return;
    } 

    for (var i = 0; i < defaultUserOptions.spawn; i++) {
        userparticles.push({
            vx: fuzzy(defaultUserOptions.initialXVelocity),
            vy: fuzzy(defaultUserOptions.initialXVelocity),
            x: mousePos.x * familyCanvasToolsModule.getPixelRatio(),
            y: mousePos.y * familyCanvasToolsModule.getPixelRatio(),
            age: 0,           
            color: { r: 67 / 255., g: 55 / 255., b: 225 / 255. }// randomFloat(0.7, 1.0) }        
        });
    }
}


function drawCircleMask() {
    if (mousePos == undefined) return;
    var _ctx1 = ctxes[0];
    _ctx1.beginPath();
    _ctx1.arc(mousePos.x, mousePos.y, maskCircleSize, 0, Math.PI * 2, true);
    _ctx1.closePath();
    _ctx1.fill();
}
return {
    init_fur: function (id, _divid) {
        init_fur(id, _divid)
    },

    destroyFur: function () {
       destroyFur();
    },

    resizefurInit: function () {
        resizefurInit();
    },
}
}());
