function FamilyPolyParticle()
{
    this.mesh;
    this.homeX;
    this.homeY;
    this.homeZ;
    this.rotX;
    this.rotY;
    this.rotZ;
    this.speed;
    this.radiusRot;

    this.forceDist = 10;
    this.forceDist2 = 100;
    this.maskEnabled = true;

    this.acceleration = new THREE.Vector3(0, 0, 0);
    this.velocity = new THREE.Vector3(0, 0, 0);

    this.unitForceVector = { x: Math.random(), y: Math.random() };
    this.flying = 0;

    this.alpha = 0.8;//this.maskEnabled ? Math.random()*0.7 + 0.6 : 1;

    this.minScale = getRandomArbitrary(1, 8);

    this.alphawasgen = false;
    this.material;
    this.signVector;

    this.appearSpeed = 5 + Math.random() * 5;
}

FamilyPolyParticle.prototype.initDefaults = function () {

    this.speed = Math.random() * 0.01;
    this.radiusRot = .032 + Math.random() * 0.01;

    this.material.opacity = 0.75;// this.alpha;
}

FamilyPolyParticle.prototype.setNormalScale = function (_scale)
{
    var scale;
    var rand = Math.random();
    scale = 0.3 + getRandomArbitrary(1.0, 3.0);
    if (rand < 0.3)
        scale = 0.3 + getRandomArbitrary(0.4, 0.8);
    else if (rand >= 0.3 && rand <= 0.6)
        scale = 0.2 + getRandomArbitrary(0.8, 1.0);
    else if (rand >= 0.6 && rand <= 0.97)
        scale = 0.3 + getRandomArbitrary(1.3, 1.7);
    else 
        scale = 0.3 + getRandomArbitrary(2.0, 6.0);

    this.mesh.scale.set(scale, scale, scale);
}

FamilyPolyParticle.prototype.setlargeScale = function ()
{
    var scale = 1 + getRandomArbitrary(1.2, 4);
    this.mesh.scale.set(scale, scale, scale);
}

FamilyPolyParticle.prototype.setRandomRotation = function ()
{
    this.mesh.rotation.set(Math.random() * 2, Math.random() * 2, Math.random() * 2);
}

FamilyPolyParticle.prototype.setXYZ = function (x, y, z)
{
    this.homeX = this.mesh.position.x = x;
    this.homeY = this.mesh.position.y = y;
    this.homeZ = this.mesh.position.z = z;
}

FamilyPolyParticle.prototype.setRot = function (x, y, z)
{
    this.rotX = this.mesh.rotation.x = x;
    this.rotY = this.mesh.rotation.y = y;
    this.rotZ = this.mesh.rotation.z = z;
}

FamilyPolyParticle.prototype.addRot = function (x, y, z) {
    this.mesh.rotation.x += x;
    this.mesh.rotation.y += y;
    this.mesh.rotation.z += z;
}

FamilyPolyParticle.prototype.applyForce = function (x, y)
{
    if (this.maskEnabled) {
        if (this.distOkA(x, y)) {
            if (this.animateIn == false) {            
                var a = new THREE.Vector2(this.homeX, this.homeY);
                var b = new THREE.Vector2(x, y);
                var vec = new THREE.Vector2(this.homeX - x, this.homeY - y);

                var _dist = this.dist(this.homeX, this.homeY, x, y);
                this.mesh.position.x = x + vec.x * 2.8;//* _dist * getRandomArbitrary(0, 2); //- 1000 * _dist;//
                this.mesh.position.x = x + getRandomArbitrary(-210, 210);
                this.mesh.position.y = y + vec.y * 2.8//_dist * getRandomArbitrary(0, 2);// - 1000 * _dist; //getRandomArbitrary(-505, 505);
                this.mesh.position.y = y + getRandomArbitrary(-210, 210);
                this.signVector = { x: Math.sign(this.mesh.position.x), y: Math.sign(this.mesh.position.y) };
                this.animateIn = true;
            }
        }
        else {
            this.animateIn = false;
            this.mesh.position.x = -10000;
        }   
    }
}

FamilyPolyParticle.prototype.update = function (time)
{
    var timeUpdt = time * this.speed;
    var x = this.radiusRot * Math.cos(timeUpdt);
    var y = this.radiusRot * Math.sin(timeUpdt);

    //this.velocity.add(this.acceleration);
    //this.mesh.position.add(this.velocity);
    //this.mesh.material.opacity = this.alpha;

    this.addRot(this.speed , this.speed , this.speed );

    //this.mesh.position.x += x;
    //this.mesh.position.y += y;

    //if (this.mesh.position.z < this.homeZ - 1700 && this.flying == 1) {
    //    this.velocity.x = 0;
    //    this.velocity.y = 0;
    //    this.velocity.z = getRandomArbitrary(0., 70.1);

    //    this.mesh.position.x = this.homeX;
    //    this.mesh.position.y = this.homeY;
    //    ////this.mesh.position.z = this.homeZ;

    //    this.acceleration.x = 0;
    //    this.acceleration.y = 0;
    //    this.acceleration.z = getRandomArbitrary(0., 70.1);
    //    this.flying = 2;       
    //}

    if (this.flying == 2)
    {
       // this.acceleration.z *= (this.homeZ - this.mesh.position.z);
        if(this.mesh.position.z  >= this.homeZ)
        {
            this.velocity.z = 0;
            this.acceleration.z = 0;
            this.flying = 0;
        }
    }

    if (this.maskEnabled && this.animateIn)
    {
        this.mesh.position.x += -this.signVector.x * (this.appearSpeed  + getRandomArbitrary(0, 2));
        this.mesh.position.y += -this.signVector.y * (this.appearSpeed*0.1 + getRandomArbitrary(2, 12));

        if (this.signVector.x > 0) {
            if (this.mesh.position.x <= this.homeX) {
                this.mesh.position.x = this.homeX;
            }
        }
        if (this.signVector.x < 0) {
            if (this.mesh.position.x >= this.homeX) {
                this.mesh.position.x = this.homeX;
            }
        }

        if (this.signVector.y > 0) {
            if (this.mesh.position.y <= this.homeY) {
                this.mesh.position.y = this.homeY;
            }
        }
        if (this.signVector.y < 0) {
            if (this.mesh.position.y >= this.homeY) {
                this.mesh.position.y = this.homeY;
            }
        } 
    }
   
   
   // console.log(this.alpha);

    //var epsilon = 100;
    //if (Math.abs(this.mesh.position.x - x) < epsilon && Math.abs(this.mesh.position.y - y) < epsilon)
    //{
    //    this.mesh.position.x = this.homeX + 100;
    //    this.mesh.position.y = this.homeY + 100;
    //}
    //else
    //{
    //    this.mesh.position.x = this.homeX;
    //    this.mesh.position.y = this.homeY;
    //}
}

FamilyPolyParticle.prototype.dist = function (x, y, x1, y1)
{
    return Math.sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1));
}

FamilyPolyParticle.prototype.distOk = function (x, y)
{
    return Math.sqrt((x - this.homeX) * (x - this.homeX) + (y - this.homeY) * (y - this.homeY)) < this.forceDist;
}

FamilyPolyParticle.prototype.dist2Ok = function (x, y) {
    return Math.sqrt((x - this.homeX) * (x - this.homeX) + (y - this.homeY) * (y - this.homeY)) < this.forceDist2;
}

FamilyPolyParticle.prototype.distOkA = function (x, y) {
    return Math.sqrt((x - this.homeX) * (x - this.homeX) + (y - this.homeY) * (y - this.homeY)) < 150;
}
