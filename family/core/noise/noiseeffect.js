var NoiseEffectModule = (function() {

var imageObj;
var idIndex;
var canvases = [];
var ctxes = [];
var divMain;
var canvasW, canvasH = 190;
var timer = new clock.Clock();
var imagescale = 1.;
var resolution = 7;

function init(_id, divId)
{
    divMain = divId;
    canvasW = document.getElementById(divMain).clientWidth; //1268;    
    imageObj = new Image();   

    imageObj.onload = function ()
    {
        var data = familyCanvasToolsModule.createCanvases(divMain, 1);
        ctxes = data.ctx;
        canvases = data.cv;
        main(canvasW, canvasH);

        window.addEventListener('mousemove', mousemove, false);
        resizeInit();
    }
   
    idIndex = _id;   
    imageObj.src = familyImageStoreModule.getImageRaw(_id);
}

function mousemove(evt)
{    
    mousePos = familyCanvasToolsModule.getMousePos(canvases[0], evt);
}

function destroy()
{
    timer.stop();    

    ctxes[0].clearRect(0, 0, canvases[0].width, canvases[0].height);  
    window.removeEventListener('mousemove', mousemove, false);

    var _div = document.getElementById(divMain);
    for (var i = 0; i < 1; i += 1) {      
            _div.removeChild(canvases[i]);
    }   
}

function resizeInit()
{
    resize(familyCanvasToolsModule.getScale());
}

function resize(_scale)
{  
    var w = imageObj.width * _scale;
    var h = imageObj.height * _scale;  

    imagescale = _scale;
    familyCanvasToolsModule.setCanvasSize(w, h, canvases);
   
    particles = [];
  
    initText();
    clearData();

    familyCanvasToolsModule.resizeCanvases();
}

function initText()
{
    var canv = canvases[0];
    var _ctx = ctxes[0];

    _ctx.drawImage(imageObj, 0, 0, imageObj.width * imagescale, imageObj.height * imagescale);
    var imageData = _ctx.getImageData(0, 0, imageObj.width * imagescale, imageObj.height * imagescale);
     
    pointParticles = [];
 //   noise = new Noise(canv.width, canv.height, 8);

    for (var x = 0; x < imageData.width; x += resolution) {
        for (var y = 0; y < imageData.height; y += resolution) {
            i = (y * imageData.width + x) * 4;
            if (imageData.data[i + 3] > 228)
            {
                //var r1 = w / 4,
                //   a1 = getRandomArbitrary(0, 2 * Math.PI),
                //   r2 = getRandomArbitrary(0, 1, true),
                //   a2 = getRandomArbitrary(0, 2 * Math.PI);

                //var pos = Vector.fromPolar(r1, a1),
                //    vel = Vector.fromPolar(r2, a2);
                //pos.add(new Vector(w / 2, h / 2));

                pointParticles.push(new Particle(x, y, getRandomArbitrary(-10, 10), getRandomArbitrary(-10, 10)));
            }
        }
    }
}

function clearData()
{
    familyCanvasToolsModule.clearCanvases(canvases, ctxes);
   
    var imgdata = ctxes[0].getImageData(0, 0, canvases[0].clientWidth, canvases[0].clientHeight);
    data = imgdata.data;
    hdrdata = new FloatArray(data.length);
    for (var i = 0; i < hdrdata.length; i++) {
        hdrdata[i] = 0;
    }
}

function main(w, h)
{
    familyCanvasToolsModule.setCanvasSize(w, h, canvases);

    timer.start();
    FloatArray = window.Float32Array || Array;
   
    initText();
    var canv = canvases[0];
    var ctx = ctxes[0];

    var noise = new Noise(canv.width, canv.height, 8)

    timer.ontick = function (td)
    {  
       for (var i = 0; i < pointParticles.length; i += 1)
       {
            // console.log("draw");
            var p = pointParticles[i];
            p.update(noise);
            p.draw(ctx);
        //    ctx.beginPath();
        //    ctx.arc(p.x, p.y, radius, 0, Math.PI * 2);
        //    ctx.fill();
        }
    }      
    clearData();
}

////////////////////////////////////////////////////////////////////////

class Vector {
    constructor(x, y) {
        this.x = x
        this.y = y
    }

    static fromPolar(r, t) {
        return new Vector(
          r * Math.cos(t),
          r * Math.sin(t))
    }

    add(v) {
        this.x += v.x
        this.y += v.y
        return this
    }

    mul(x) {
        this.x *= x
        this.y *= x
        return this
    }

    dist(v) {
        let dx, dy
        return Math.sqrt(
          (dx = this.x - v.x) * dx,
          (dy = this.y - v.y) * dy)
    }

    get mag() {
        return Math.sqrt(
          this.x * this.x,
          this.y * this.y)
    }

    set mag(v) {
        let n = this.norm()
        this.x = n.x * v
        this.y = n.y * v
    }

    norm() {
        let mag = this.mag
        return new Vector(
          this.x / mag, this.y / mag)
    }
}

class Noise 
{
    constructor (w, h, oct)
    {
        this.width = w
        this.height = h
        this.octaves = oct
        this.canvas = canvases[0];
        let ctx = ctxes[0];
        this.data = ctx.getImageData(0, 0, w, h).data
    }
  
    // create w by h noise
    static noise (w, h)
    {
        let cv = canvases[0],
            ctx = ctxes[0];
    
        cv.width = w
        cv.height = h
    
        let img = ctx.getImageData(0, 0, w, h),
            data = img.data
    
        for (let i = 0, l = data.length; i < l; i += 4) {
            data[i + 0] = getRandomArbitrary(0, 255)
            data[i + 1] = getRandomArbitrary(0, 255)
            data[i + 2] = getRandomArbitrary(0, 255)
            data[i + 3] = 255
        }
    
        ctx.putImageData(img, 0, 0)
        return cv;
    }
  
    // create composite noise with multiple octaves
    static compositeNoise (w, h, oct)
    {
        let cv = canvases[0],
            ctx = ctxes[0];
    
        cv.width = w
        cv.height = h
    
        ctx.fillStyle = '#000'
        ctx.fillRect(0, 0, w, h)
    
        ctx.globalCompositeOperation = 'lighter'
        ctx.globalAlpha = 1 / oct
    
        for (let i = 0; i < oct; i++) {
            let noise = Noise.noise(w >> i, h >> i)
            ctx.drawImage(noise, 0, 0, w, h)
        }
    
        return cv
    }
  
    // returns noise from -1.0 to 1.0
    getNoise (x, y, ch) 
    {
        // bitwise ~~ to floor
        let i = (~~x + ~~y * this.width) * 4
        return this.data[i + ch] / 127 - 1
    }
}

class Particle 
{
         constructor (x, y, vx, vy) 
        {
            this.pos = new Vector(x, y)
            this.vel = new Vector(vx, vy)
            this.acc = new Vector(0, 0)
            // this.tick = 0
            // this.life = random(100, 300)
        }
  
        update(noise)
        {
            // this.tick++
            // if (this.tick > this.life)
            //   return
      
            this.pos.add(this.vel)
    
            var x = this.pos.x;
            var y = this.pos.y;
            let dx = noise.getNoise(x, y, 0),
                dy = noise.getNoise(x, y, 1) 
    
            // this.vel.add(this.acc)
            this.vel.add(new Vector(dx, dy))
            // this.acc.add(new Vector(dx / 10, dy / 10))
            // this.acc.mul(0.95)
            this.vel.mul(0.95)
        }
  
        draw(ctx)
        {
            // if (this.tick > this.life) return
            ctx.fillRect(this.pos.x, this.pos.y, 2, 2)
        }
    }



return {
    init: function (id, divid) {
        init(id, divid)
    },

    destroy: function () {
       destroy();
    },

    resizeInit: function () {
        resizeInit();
    },
}
}());
