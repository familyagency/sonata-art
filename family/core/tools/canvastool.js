var familyCanvasToolsModule = (function() {

var letterspacing = 10;
var pixelRatio = Math.min(2.0, window.devicePixelRatio || 1.0);

function getPixelRatio()
{
    return pixelRatio;
}

function createCanvases(divMain, count)
{
    var canvases = [];
    var ctxes = [];
   
    for (var i = 0 ; i < count; i++) {       
        canvases.push(document.createElement('canvas'));
        canvases[i].className = "canvas";
        document.getElementById(divMain).appendChild(canvases[i]);
        ctxes.push(canvases[i].getContext('2d'));
        canvases[i].id = "id" + i;
       // canvases[i].style.letterSpacing = letterspacing + 'px';
    }
    return { cv: canvases, ctx: ctxes };
}

function setCanvasSize(w, h, canvases) {   

    for (var i = 0 ; i < canvases.length; i++) {
        canvases[i].width = w * pixelRatio;
        canvases[i].style.width = w + 'px';

        canvases[i].height = h * pixelRatio;
        canvases[i].style.height = h + 'px';
    }
}

function getScale()
{
    var scale = 1.;
    var maxWidth = 1920;
    var midWidth = 1600;
    var scale = 1.;

    if (window.innerWidth > maxWidth) {
        scale = 1.;
    }
    else if (window.innerWidth < maxWidth && window.innerWidth > 1600) {
        scale = 1.;
    }
    else if (window.innerWidth < 1600 && window.innerWidth > 1200) {
        scale = 910 / 1170.;
    }
    else {
        scale = window.innerWidth / maxWidth;
    }

    return scale;
}

function getMousePos(canvas, evt)
{
    var rect = canvas.getBoundingClientRect();
    return { x: evt.clientX - rect.left, y: evt.clientY - rect.top };
}

function resizeCanvases()
{
    var containerwrap = document.getElementsByClassName('canvas');

    for (var i = 0; i < containerwrap.length; i++) {
        var cwidth = containerwrap[i].clientWidth;
        var cheight = containerwrap[i].clientHeight;

        containerwrap[i].style.marginLeft = -(0.5 * cwidth) + "px";

        containerwrap[i].setAttribute("width", cwidth + "px");
        containerwrap[i].setAttribute("height", cheight + "px");
    }
}

function clearCanvases(canvasesFont, ctxesFont) {
    for (var i = 0 ; i < canvasesFont.length; i++) {
        ctxesFont[i].globalCompositeOperation = 'source-over';
        ctxesFont[i].fillStyle = "rgba(255, 0, 0)";

        ctxesFont[i].globalAlpha = 0.;
        ctxesFont[i].fillRect(0, 0, canvasesFont[i].clientWidth, canvasesFont[i].clientHeight);
        ctxesFont[i].globalAlpha = 1.0;
    }
}

function copyImageData(ctx, src)
{
    var dst = ctx.createImageData(src.width, src.height);
    dst.data.set(src.data);
    return dst;
}

return {
    copyImageData: function (ctx, src) {
        return copyImageData(ctx, src)
    },

    clearCanvases: function (a, b) {
        clearCanvases(a, b)
    },

    resizeCanvases: function () {
        resizeCanvases()
    },

    getMousePos: function (a, b) {
        return getMousePos(a, b)
    },

    getScale: function () {
        return getScale()
    },

    setCanvasSize: function (w, h, canvases) {
        setCanvasSize(w, h, canvases)
    },

    createCanvases: function (a, b) {
        return createCanvases(a, b)
    },

    getPixelRatio: function () {
        return getPixelRatio()
    },
}
}());