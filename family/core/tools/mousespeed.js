var familyMouseModule = (function() {

var lastMouseX = null;
var lastMouseY = null;
var speedX = 0;
var speedY = 0;

var directionX = 0;
var directionY = 0;

var timestamp = null;

function checkSpeed(x, y)
{
    if (timestamp === null) 
    {
        timestamp = Date.now();
        lastMouseX = x;
        lastMouseY = y;
        return;
    }

    var now = Date.now();
    var dt = now - timestamp;
    var dx = directionX = x - lastMouseX;
    var dy = directionY = y - lastMouseY;
    speedX = Math.round(dx / dt * 100);

    speedY = Math.round(dy / dt * 100);

    timestamp = now;
    lastMouseX = x;
    lastMouseY = y;    
}

return {
    checkSpeed: function (x, y) {
        checkSpeed(x, y);
    },
    getSpeedX: function ()
    {
        return speedX;
    },
    getSpeedY: function ()
    {
        return speedY;
    }
}
}());