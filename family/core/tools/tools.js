function mapInterval(val, A, B, a, b) {
    return (val - A) * (b - a) / (B - A) + a;
}

function mapIntervalClamp(val, A, B, a, b) {
    if (val < A) val = a;
    if (val > B) val = b;
    return (val - A) * (b - a) / (B - A) + a;
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function shuffleArray(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function writepixels(pixels) {
    var pixelstr = "[";
    for (var j = 0; j < pixels.length; j++) {
        pixelstr += "{homeX:" + pixels[j].homeX + ", homeY:" + pixels[j].homeY + "},";
    }
    pixelstr = pixelstr.substring(0, pixelstr.length - 1);
    pixelstr += "]";

    function saveTextAsFile() {
        var textToSave = pixelstr;
        var textToSaveAsBlob = new Blob([textToSave], { type: "text/plain" });
        var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);

        var downloadLink = document.createElement("a");
        downloadLink.download = "pixels.txt";
        downloadLink.innerHTML = "Download File";
        downloadLink.href = textToSaveAsURL;
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);

        downloadLink.click();
    }
    saveTextAsFile();
}

function randomInteger(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}

function randomFloat(min, max) {
    return Math.random() * (max - min) + min;
}

function clamp(val, min, max)
{
   return Math.min(Math.max(min, val), max);
}